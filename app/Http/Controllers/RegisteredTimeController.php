<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegisteredTime;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RegisteredTimeController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    //
    // Busca no banco de dados os registros do usuário autenticado no sistema
    //
    public function get_registers()
    {
        $user_id = auth()->guard('user')->getUser()->id;

        $registers = DB::table('registered_time')
                        ->join('user', 'registered_time.user_id', '=','user.id')
                        ->where('user.id', '=', $user_id)
                        ->select('registered_time.id', 'registered_time.time_registered', 'user.name as user_name')
                        ->get();

        return json_encode($registers->jsonSerialize(), Response::HTTP_OK);
    }

    //
    // Busca no banco de dados todos os registros caso o usuário autenticado seja administrador
    //
    public function get_all_registers()
    {
        $user_role = auth()->guard('user')->getUser()->role;

        if ($user_role == 2) {
            
            $registers = DB::table('registered_time')
                            ->join('user', 'registered_time.user_id', '=','user.id')
                            ->select('registered_time.id', 'registered_time.time_registered', 'user.name as user_name')
                            ->orderBy('registered_time.id')
                            ->get();

            return json_encode($registers->jsonSerialize(), Response::HTTP_OK);
        } else {
            return json_encode(null, Response::HTTP_OK);
        }
    }
    
    //
    // Retorna todos os registros cadastrados para a rota de API
    //
    public function get_all_registers_api()
    {
        return json_encode(RegisteredTime::all()->jsonSerialize(), Response::HTTP_OK);
    }

    //
    // Salva um novo registro no banco de dados e envia para o canal do Pusher
    //
    public function store(Request $request)
    {
        $registered_time = new RegisteredTime();
        $registered_time->time_registered = Carbon::parse($request->date)->timezone("America/Sao_Paulo");
        $registered_time->user_id = auth()->guard('user')->getUser()->id;
        $registered_time->save();

        event(new \App\Events\CreateRegisteredTime($registered_time));

        return response($registered_time->jsonSerialize(), Response::HTTP_CREATED);
    }
}
