<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'João',
            'email' => 'joao@gmail.com',
            'password' => bcrypt("12345678"),
            'role' => 2
        ]);

        DB::table('user')->insert([
            'name' => 'Paulo',
            'email' => 'paulo@gmail.com',
            'password' => bcrypt("1234567"),
            'role' => 1
        ]);

        DB::table('user')->insert([
            'name' => 'Pedro',
            'email' => 'pedro@gmail.com',
            'password' => bcrypt("123456"),
            'role' => 1
        ]);
    }
}
