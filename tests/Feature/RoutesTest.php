<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoutesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_routes()
    {
        $response = $this->get('/');
        $response->assertStatus(200);

        $response = $this->post('/user/login');
        $response->assertStatus(302);

        $response = $this->get('/user/logout');
        $response->assertStatus(302);

        $response = $this->get('/get_user');
        $response->assertStatus(302);

        $response = $this->get('/registers');
        $response->assertStatus(302);

        $response = $this->get('/all_registers');
        $response->assertStatus(302);

        $response = $this->post('/create_register');
        $response->assertStatus(302);

        $response = $this->get('/api/all_registers');
        $response->assertStatus(200);
    }
}
