<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */

    //
    // Retorna a página do usuário
    //
    public function index()
    {
        return view('index');
    }

    //
    // Retorna o usuário autenticado no sistema
    //
    public function get_user()
    {
        $user = auth()->guard('user')->getUser();

        return response($user->jsonSerialize(), Response::HTTP_OK);
    }

    //
    // Verifica as credenciais do usuário e efetua o login
    //
    public function login(Request $request)
    {
        $data = ['email' => $request->get('email'), 'password' => $request->get('password')];
        
        if (Auth()->guard('user')->attempt($data)) {
            return redirect('/user');
        } else {
            return redirect('/')
                ->withInput();
        }
    }

    //
    // Faz logout no sistema
    //
    public function logout()
    {
        auth()->guard('user')->logout();
        return redirect('/');
    }
}
