<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'user'], function () {

    Route::get('/', function () {
        return view('/auth/login');
    });

    Route::post('/user/login', 'UserController@login')->name('user.login');

    Route::group(['middleware' => 'auth:user'], function () {
        Route::get('/user', 'UserController@index');
        Route::get('/user/logout', 'UserController@logout');
        Route::get('/get_user', 'UserController@get_user');
        Route::get('/registers', 'RegisteredTimeController@get_registers');
        Route::get('/all_registers', 'RegisteredTimeController@get_all_registers');
        Route::post('/create_register', 'RegisteredTimeController@store');
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
