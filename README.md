<h1><b>Time Register</b></h1>

## Tecnologias Utilizadas

### Front-End

- [W3.CSS](https://www.w3schools.com/w3css/defaulT.asp)

- [Vue.JS](https://vuejs.org/)

### Back-End

- [Laravel 8](https://laravel.com/)

- [Mysql](https://www.mysql.com/)

### RealTime

- [Pusher](https://pusher.com/)

### Manipulação de datas

- [Carbon](https://carbon.nesbot.com/)

### Testes

- [PHPUnit](https://phpunit.de/)

## Instruções para execução

### Clonar projeto

```
git clone git@gitlab.com:andremaia167/register.git

ou

git clone https://gitlab.com/andremaia167/register.git
```

### Instalar dependências

```
composer install

npm install
```

### Criar arquivo .env

```
cp .env.example .env
```

### Criar e configurar banco de dados

Criar banco de dados com o nome de 'register' (ou qualquer outro nome)

Em **.env:**

```
DB_DATABASE=register
```

No Terminal:

```
php artisan migrate

php artisan db:seed
```

### Configurar acesso ao Pusher

Em **.env:**

```
BROADCAST_DRIVER=pusher
```

```
PUSHER_APP_ID=1166495
PUSHER_APP_KEY=2391b1cb0e0244b74b2b
PUSHER_APP_SECRET=f3b12b6604a40443c0dd
```

### Gerar chave de projeto e iniciar servidor

```
php artisan key:generate

php artisan serve
```

### Usuários

Email: joao@gmail.com (admin)
<br>
Senha: 12345678

Email: paulo@gmail.com
<br>
Senha: 1234567

Email: pedro@gmail.com
<br>
Senha: 123456

### Execução de testes

```
./vendor/bin/phpunit

ou

./vendor/bin/phpunit --filter RoutesTest
```
