<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisteredTime extends Model
{
    use HasFactory;

    protected $table = 'registered_time';

    public $timestamps = false;
}
