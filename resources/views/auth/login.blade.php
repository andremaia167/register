<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Register - Login</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">

        <!-- Logo e título do sistema -->
        <div id="title">
            <i class="material-icons" style="font-size: 80px">access_time</i>
            <h1><b>Time Register</b></h1>
        </div>
        
        <!-- Formulário de login -->
        <div id="form_login" class="w3-card-4">
            <div class="w3-container layout_color" style="margin-bottom: 10px">
                <h3 id="text_form_login_header">Login</h3>
            </div>
            <form class="w3-container" method="POST" action="{{ route('user.login') }}">
                @csrf
                <p>      
                <label class="w3-text"><b>E-Mail</b></label>
                <input class="w3-input w3-border" name="email" type="text"></p>
                <p>      
                <label class="w3-text"><b>Senha</b></label>
                <input class="w3-input w3-border" name="password" type="password"></p>
                <p>
                <button class="w3-btn layout_color">Login</button></p>
            </form>
        </div>
    </div>
</body>
